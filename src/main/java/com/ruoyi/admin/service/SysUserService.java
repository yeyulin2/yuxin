package com.ruoyi.admin.service;

import com.ruoyi.admin.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 14717
* @description 针对表【sys_user(用户信息表)】的数据库操作Service
* @createDate 2024-09-11 14:49:34
*/
public interface SysUserService extends IService<SysUser> {

}
