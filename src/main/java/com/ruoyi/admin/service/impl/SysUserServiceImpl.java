package com.ruoyi.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.admin.entity.SysUser;
import com.ruoyi.admin.service.SysUserService;
import com.ruoyi.admin.mapper.SysUserMapper;
import org.springframework.stereotype.Service;

/**
* @author 14717
* @description 针对表【sys_user(用户信息表)】的数据库操作Service实现
* @createDate 2024-09-11 14:49:34
*/
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser>
    implements SysUserService{

}




