package com.ruoyi.admin.mapper;

import com.ruoyi.admin.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 14717
* @description 针对表【sys_user(用户信息表)】的数据库操作Mapper
* @createDate 2024-09-11 14:49:34
* @Entity com.ruoyi.admin.entity.SysUser
*/
public interface SysUserMapper extends BaseMapper<SysUser> {

}




